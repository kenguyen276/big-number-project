﻿#pragma once

#include "Header.h"

class DInt
{
private:

	// Mảng kiểu nguyên 2 phần tử tương đương 64 bit
	int data[2]; 
public:

	// Hàm khởi tạo mặc định
	DInt(void); 

	// Hàm hủy mặc định
	~DInt(void);

	// Hàm nhập giá trị từ chuỗi
	void Scan(string Str);

	// Hàm xuất giá trị
	string Print();

	// Hàm chuyển sang bù 2
	void ToNegative();

	// Hàm chuyển từ số âm bù 2 sang dương
	void ToPositive();

	// Hàm chuyển từ hệ thập phân sang nhị phân
	string DecToBin();

	// Hàm chuyển từ hệ nhị phân sang thập phân
	void BinToDec(string Str);

	// Hàm chuyển từ hệ nhị phân sang thập lục phân
	string BinToHex(string Str);

	// Hàm chuyển từ hệ thập phân sang thập lục phân
	string DecToHex();

	// Toán tử cộng
	DInt operator+(const DInt &b);

	// Toán tử trừ
	DInt operator-(const DInt &b);

	// Toán tử nhân
	DInt operator*(const DInt &b);

	// Toán tử chia
	DInt operator/(const DInt &b);

	// Hàm reset giá trị về 0
	void Reset();
};


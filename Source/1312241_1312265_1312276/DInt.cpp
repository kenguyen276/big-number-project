﻿#include "DInt.h"


// Hàm khởi tạo mặc định gán các phần tử có giá trị là 0
DInt::DInt(void)
{
	data[0] = data[1] = 0;
}

// Hàm hủy mặc định
DInt::~DInt(void)
{
	return;
}

// Hàm nhập giá trị từ chuỗi
void DInt::Scan(string Str)
{
	// Biến đánh dấu là một số âm
	bool IsNegative = false;

	// Nếu là số âm thì đánh dấu
	if (Str[0] == '-')
	{
		Str[0] = '0';
		IsNegative = true;
	}


	for (int i = 63; Str != ""; i--)
	{
		// Kiểm tra số dư phép chia cho 2 của chữ số cuối cùng
		int bit = (Str[Str.length() - 1] - 48) % 2;

		// Thực hiện gán bit 
		if (i < 32)
		{
			bit = bit << (31 - i);
			data[0] = data[0] | bit;
		}
		else
		{
			bit = bit << (63 - i);
			data[1] = data[1] | bit;
		}

		// Chia chuỗi cho 2
		string Thuong;
		int Du = 0;
		for (int j = 0; j < Str.length(); j++)
		{
			int GiaTri = (Str[j] - 48) + 10 * Du;
			int T = GiaTri / 2;
			if (T || j != 0 || i == (Str.length() - 1) )
				Thuong.push_back(T + 48);
			Du = GiaTri % 2;
		}

		Str = Thuong;
	}
	// Nếu là số âm
	if (IsNegative)
		ToNegative();
}

// Hàm xuất giá trị thập phân
string DInt::Print()
{
	string Dec = "0"; // Biến lưu kết quả ở dạng chuỗi

	// Kiểm tra là số âm hay dương
	// Nếu bit đầu là 1 thì là số âm
	// Bit đầu là 0 thì là số dương
	bool IsNegative = false;
	if (data[0] >> 31 & 1) 
	{
		// Nếu là số âm thì chuyển dương
		this->ToPositive();

		// Đánh dấu là số âm
		IsNegative = true;
	}

	// Tính giá trị của số bù 1
	for (int i = 63; i > 0; i--)
	{
		// Từ bit 32 trở về
		if (i < 32)
		{
			if (((data[0] >> (31 - i)) & 1))
			{
				string T = "1";
				// Tính giá trị lũy thừa 2 sau đó cộng lại
				for (int j = 0; j < 63 - i; j++)
					T = StrMult2(T);
				Dec = StrAddStr(Dec, T);
			}
		} 
		// Từ bit 33 đến 64
		else 
		{
			if (((data[1] >> (63 - i)) & 1))
			{
				string T = "1";
				// Tính giá trị lũy thừa 2 sau đó cộng lại
				for (int j = 0; j < 63 - i; j++)
					T = StrMult2(T);
				Dec = StrAddStr(Dec, T);
			}
		}
	}

	// Nếu là số âm thì thêm dấu -
	if (IsNegative)
	{
		Dec.insert(0, "-");
		ToNegative();
	}

	if (Dec == "-0")
		Dec = "-9223372036854775808";
	// Xuất kết quả ra màn hình
	return Dec;
}

// Hàm chuyển sang số âm dạng bù 2
void DInt::ToNegative()
{
	// Đảo bit
	data[0] = ~data[0];
	data[1] = ~data[1];


	// Cộng thêm 1
	if (data[1] == -1)
	{
		data[0] += 1;
		data[1] += 1;
	}
	else
		data[1] += 1;
}

// Hàm chuyển sang số dương từ số bù 2
void DInt::ToPositive()
{
	DInt A, B;

	A.data[0] = data[0];
	A.data[1] = data[1];

	B.Scan("1");
	// Trừ đi 1
	A = A - B;

	// Đảo dấu
	data[0] = ~A.data[0];
	data[1] = ~A.data[1];
}

// Hàm chuyển từ hệ thập phân sang nhị phân
string DInt::DecToBin()
{
	string Bin;
	for (int i = 0; i < 64; i++)
		if (i < 32)
		{
			int bit = ((data[0] >> (31 - i)) & 1);
			Bin.push_back(bit + 48);
		}
		else 
		{
			int bit = ((data[1] >> (63 - i)) & 1);
			Bin.push_back(bit + 48);
		}
		return Bin;
}

// Hàm chuyển từ hệ nhị phân sang thập phân
void DInt::BinToDec(string Str)
{
	int bit;
	for (int i = 0; i < 64; i++)
	{
		if (i < 32)
		{
			if (Str[i] == '1')
				bit = 1;
			else 
				bit = 0;
			data[0] = data[0] << 1;
			data[0] = data[0] | bit;
		}
		else
		{
			if (Str[i] == '1')
				bit = 1;
			else 
				bit = 0;
			data[1] = data[1] << 1;
			data[1] = data[1] | bit;
		}
	}
}

// Hàm chuyển từ hệ nhị phân sang thập lục phân
string DInt::BinToHex(string Str)
{
	string Hex;
	for (int i = 63; i >= 0; i = i - 4)
	{
		int H = 0;
		if (i >= 0 && Str[i] == '1')
			H += 1;
		if (i - 1 >= 0 && Str[i - 1] == '1')
			H += 2;
		if (i - 2 >= 0 && Str[i - 2] == '1')
			H += 4;
		if (i - 3 >= 0 && Str[i - 3] == '1')
			H += 8;
		if (H < 10)
			Hex.push_back(H + 48);
		else
		{
			switch (H)
			{
			case 10:
				Hex.push_back('A');
				break;
			case 11:
				Hex.push_back('B');
				break;
			case 12:
				Hex.push_back('C');
				break;
			case 13:
				Hex.push_back('D');
				break;
			case 14:
				Hex.push_back('E');
				break;
			case 15:
				Hex.push_back('F');
				break;
			}
		}
	}
	for (int i = 0; i < Hex.size() / 2; i++)
		swap(Hex[i], Hex[Hex.size() - 1 - i]);
	return Hex;
}

// Hàm chuyển từ hệ thập phân sang thập lục phân
string DInt::DecToHex()
{
	string Bin = DecToBin();
	string Hex = BinToHex(Bin);
	return Hex;
}

// Toán tử cộng
DInt DInt::operator+(const DInt &b)
{
	DInt S;
	int Nho = 0;
	for (int i = 63; i >= 0; i--)
	{
		if (i < 32)
		{
			int bita = (data[0] >> (31 - i)) & 1;
			int bitb = (b.data[0] >> (31 - i)) & 1;
			int bit = bita + bitb + Nho;
			if (bit == 2)
				bit = 0;
			else if (bit == 3)
				bit = 1;
			S.data[0] = S.data[0] | (bit << (31 - i));
			if (bita == 1 && bitb == 1 || bita == 1 && Nho == 1 || Nho == 1 && bitb == 1)
				Nho = 1;
			else 
				Nho = 0;
		}
		else
		{
			int bita = (data[1] >> (63 - i)) & 1;
			int bitb = (b.data[1] >> (63 - i)) & 1;
			int bit = bita + bitb + Nho;
			if (bit == 2)
				bit = 0;
			else if (bit == 3)
				bit = 1;
			S.data[1] = S.data[1] | (bit << (63 - i));

			if (bita == 1 && bitb == 1 || bita == 1 && Nho == 1 || Nho == 1 && bitb == 1)
				Nho = 1;
			else 
				Nho = 0;
		}
	}
	return S;
}

// Toán tử trừ
DInt DInt::operator-(const DInt &b)
{
	DInt S;
	DInt B;
	S.data[0] = data[0];
	S.data[1] = data[1];
	B = b;
	B.ToNegative();
	S = S + B;
	return S;
}

// Toán tử nhân
DInt DInt::operator*(const DInt &b)
{
	DInt S;
	DInt T;
	DInt B;

	bool KhacDau = false;

	B = b;

	T.data[0] = data[0];
	T.data[1] = data[1];

	if (T.data[0] >> 31 == -1 && B.data[0] >> 31 == -1)
	{
		T.ToPositive();
		B.ToPositive();
	}
	else if (T.data[0] >> 31 == -1)
	{
		T.ToPositive();
		KhacDau = true;
	}
	else if (B.data[0] >> 31 == -1)
	{
		B.ToPositive();
		KhacDau = true;
	}

	for (int i = 63; i >= 0; i--)
	{
		if (i < 32)
		{
			int bitb = (B.data[0] >> (31 - i)) & 1;
			int bit = T.data[1] >> 31 & 1;
			if (bitb)
				S = S + T;
			T.data[0] = (T.data[0] << 1) | bit;
			T.data[1] = T.data[1] << 1;

		}
		else
		{
			int bitb = (B.data[1] >> (63 - i)) & 1;
			int bit = T.data[1] >> 31 & 1;
			if (bitb)
				S = S + T;
			T.data[0] = (T.data[0] << 1) | bit;
			T.data[1] = T.data[1] << 1;

		}
	}
	if (KhacDau)
		S.ToNegative();
	return S;
}

// Toán tử chia
DInt DInt::operator/(const DInt &b)
{

	DInt Q;
	DInt M;

	DInt S;
	S.Scan("1");
	DInt A;

	Q.data[0] = data[0];
	Q.data[1] = data[1];

	M.data[0] = b.data[0];
	M.data[1] = b.data[1];

	bool KhacDau = false;

	// Nếu là số âm thì đổi lại thành dương
	// Ghi lại 2 số cùng dấu hay khác dấu
	if (Q.data[0] >> 31 == -1 && M.data[0] >> 31 == -1)
	{
		Q.ToPositive();
		M.ToPositive();
	}
	else if (Q.data[0] >> 31 == -1)
	{
		Q.ToPositive();
		KhacDau = true;
	}
	else if (M.data[0] >> 31 == -1)
	{
		M.ToPositive();
		KhacDau = true;
	}

	for (int i = 64; i > 0; i--)
	{
		int bit = Q.data[0] >> 31 & 1;
		Q.data[0] = (Q.data[0] << 1) | ((Q.data[1] >> 31) & 1);
		Q.data[1] = Q.data[1] << 1;
		A.data[0] = (A.data[0] << 1) | ((A.data[1] >> 31) & 1);
		A.data[1]  = (A.data[1] << 1) | bit;
		A = A - M;
		if (A.data[0] >> 31 == -1)
		{
			Q.data[1] = (Q.data[1] >> 1) ;
			Q.data[1] = (Q.data[1] << 1);
			A = A + M;
		}
		else
		{
			Q.data[1] = Q.data[1] | 1;
		}
	}
	if (KhacDau)
		Q.ToNegative();
	return Q;
}

// Hàm reset giá trị về 0
void DInt::Reset()
{
	data[0] = data[1] = 0;
}
﻿#pragma once

#include "Header.h"

class QInt
{
private:

	// Mảng kiểu nguyên 4 phần tử tương đương 128 bit
	int data[4]; 
public:

	// Hàm khởi tạo mặc định
	QInt(void); 

	// Hàm hủy mặc định
	~QInt(void);

	// Hàm nhập giá trị từ chuỗi
	void Scan(string Str);

	// Hàm xuất giá trị
	string Print();

	// Hàm chuyển sang bù 2
	void ToNegative();

	// Hàm chuyển từ số âm bù 2 sang dương
	void ToPositive();

	// Hàm chuyển từ hệ thập phân sang nhị phân
	string DecToBin();

	// Hàm chuyển từ hệ nhị phân sang thập phân
	void BinToDec(string Str);

	// Hàm chuyển từ hệ nhị phân sang thập lục phân
	string BinToHex(string Str);

	// Hàm chuyển từ hệ thập phân sang thập lục phân
	string DecToHex();

	// Toán tử cộng
	QInt operator+(const QInt &b);

	// Toán tử trừ
	QInt operator-(const QInt &b);

	// Toán tử nhân
	QInt operator*(const QInt &b);

	// Toán tử chia
	QInt operator/(const QInt &b);

	// Hàm reset về 0
	void Reset();
};


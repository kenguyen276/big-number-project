﻿#include "QInt.h"



// Hàm khởi tạo mặc định
QInt::QInt(void)
{
	data[0] = data[1] = data[2] = data[3] = 0;
}

// Hàm hủy mặc định
QInt::~QInt(void)
{
	return;
}

// Hàm nhập giá trị từ chuỗi
void QInt::Scan(string Str)
{
	// Biến đánh dấu là một số âm
	bool IsNegative = false;

	// Nếu là số âm thì đánh dấu
	if (Str[0] == '-')
	{
		Str[0] = '0';
		IsNegative = true;
	}


	for (int i = 127; Str != ""; i--)
	{
		// Kiểm tra số dư phép chia cho 2 của chữ số cuối cùng
		int bit = (Str[Str.length() - 1] - 48) % 2;

		// Thực hiện gán bit 
		if (i < 32)
		{
			bit = bit << (31 - i);
			data[0] = data[0] | bit;
		}
		else if (i < 64)
		{
			bit = bit << (63 - i);
			data[1] = data[1] | bit;
		}
		else if (i < 96)
		{
			bit = bit << (95 - i);
			data[2] = data[2] | bit;
		}
		else
		{
			bit = bit << (127 - i);
			data[3] = data[3] | bit;
		}

		// Chia chuỗi cho 2
		string Thuong;
		int Du = 0;
		for (int j = 0; j < Str.length(); j++)
		{
			int GiaTri = (Str[j] - 48) + 10 * Du;
			int T = GiaTri / 2;
			if (T || j != 0 || i == (Str.length() - 1) )
				Thuong.push_back(T + 48);
			Du = GiaTri % 2;
		}

		Str = Thuong;
	}
	// Nếu là số âm
	if (IsNegative)
		ToNegative();
}

// Hàm xuất giá trị
string QInt::Print()
{
	string Dec = "0"; // Biến lưu kết quả ở dạng chuỗi

	// Kiểm tra là số âm hay dương
	// Nếu bit đầu là 1 thì là số âm
	// Bit đầu là 0 thì là số dương

	bool IsNegative = false;
	if (data[0] >> 31 & 1) 
	{
		// Nếu là số âm thì chuyển về bù 1
		// Thực hiện trừ đi 1
		if (data[1] == 0 && data[2] == 0 && data[3] == 0) 
		{
			data[1] = ~data[1];
			data[2] = ~data[2];
			data[3] = ~data[3];
			data[0] = data[0] - 1;
		}
		else if (data[2] == 0 && data[3] == 0)
		{
			data[2] = ~data[2];
			data[3] = ~data[3];
			data[1] = data[1] - 1;
		}
		else if (data[3] == 0)
		{
			data[3] = ~data[3];
			data[2] = data[2] - 1;
		}
		else data[3] = data[3] - 1;

		// Đảo tất cả các bit
		data[0] = ~data[0];
		data[1] = ~data[1];
		data[2] = ~data[2];
		data[3] = ~data[3];

		// Đánh dấu là số âm
		IsNegative = true;
	}

	// Tính giá trị của số dương
	for (int i = 127; i > 0; i--)
	{
		// Từ bit 32 trở về
		if (i < 32)
		{
			if (((data[0] >> (31 - i)) & 1))
			{
				string T = "1";
				// Tính giá trị lũy thừa 2 sau đó cộng lại
				for (int j = 0; j < 127 - i; j++)
					T = StrMult2(T);
				Dec = StrAddStr(Dec, T);
			}
		} 
		// Từ bit 33 đến 64
		else if (i < 64)
		{
			if (((data[1] >> (63 - i)) & 1))
			{
				string T = "1";
				// Tính giá trị lũy thừa 2 sau đó cộng lại
				for (int j = 0; j < 127 - i; j++)
					T = StrMult2(T);
				Dec = StrAddStr(Dec, T);
			}
		}
		else if (i < 96)
		{
			if (((data[2] >> (95 - i)) & 1))
			{
				string T = "1";
				// Tính giá trị lũy thừa 2 sau đó cộng lại
				for (int j = 0; j < 127 - i; j++)
					T = StrMult2(T);
				Dec = StrAddStr(Dec, T);
			}
		}
		else
		{
			if (((data[3] >> (127 - i)) & 1))
			{
				string T = "1";
				// Tính giá trị lũy thừa 2 sau đó cộng lại
				for (int j = 0; j < 127 - i; j++)
					T = StrMult2(T);
				Dec = StrAddStr(Dec, T);
			}
		}
	}

	// Nếu là số âm thì thêm dấu -
	if (IsNegative)
	{
		Dec.insert(0, "-");
		ToNegative();
	}

	if (Dec == "-0")
		Dec = "-170141183460469231731687303715884105728";
	// Kết quả trả về
	return Dec;
}

// Hàm chuyển sang bù 2
void QInt::ToNegative()
{
	// Đảo bit
	data[0] = ~data[0];
	data[1] = ~data[1];
	data[2] = ~data[2];
	data[3] = ~data[3];


	// Cộng thêm 1
	if (data[1] == -1 && data[2] == -1 && data[3] == -1)
	{
		data[0] += 1;
		data[1] += 1;
		data[2] += 1;
		data[3] += 1;
	}
	else if (data[2] == -1 && data[3] == -1)
	{
		data[2] += 1;
		data[3] += 1;
		data[1] += 1;
	}
	else if (data[3] == -1)
	{
		data[3] += 1;
		data[2] += 1;
	}
	else
		data[3] += 1;
}

// Hàm chuyển số bù 2 sang dương
void QInt::ToPositive()
{
	QInt A, B;
	A.data[0] = data[0];
	A.data[1] = data[1];
	A.data[2] = data[2];
	A.data[3] = data[3];

	B.Scan("1");
	A = A - B;
	data[0] = ~A.data[0];
	data[1] = ~A.data[1];
	data[2] = ~A.data[2];
	data[3] = ~A.data[3];
}

// Hàm chuyển từ hệ thập phân sang nhị phân
string QInt::DecToBin()
{
	string Bin;
	for (int i = 0; i < 128; i++)
	{
		if (i < 32)
		{
			int bit = ((data[0] >> (31 - i)) & 1);
			Bin.push_back(bit + 48);
		}
		else if (i < 64)
		{
			int bit = ((data[1] >> (63 - i)) & 1);
			Bin.push_back(bit + 48);
		}
		else if (i < 96)
		{
			int bit = ((data[2] >> (95 - i)) & 1);
			Bin.push_back(bit + 48);
		}
		else
		{
			int bit = ((data[3] >> (127 - i)) & 1);
			Bin.push_back(bit + 48);
		}
	}
	return Bin;
}

// Hàm chuyển từ hệ nhị phân sang thập phân
void QInt::BinToDec(string Str)
{
	int bit;
	for (int i = 0; i < 128; i++)
	{
		if (i < 32)
		{
			if (Str[i] == '1')
				bit = 1;
			else 
				bit = 0;
			data[0] = data[0] << 1;
			data[0] = data[0] | bit;
		}
		else if (i < 64)
		{
			if (Str[i] == '1')
				bit = 1;
			else 
				bit = 0;
			data[1] = data[1] << 1;
			data[1] = data[1] | bit;
		}
		else if (i < 96)
		{
			if (Str[i] == '1')
				bit = 1;
			else 
				bit = 0;
			data[2] = data[2] << 1;
			data[2] = data[2] | bit;
		}
		else
		{
			if (Str[i] == '1')
				bit = 1;
			else 
				bit = 0;
			data[3] = data[3] << 1;
			data[3] = data[3] | bit;
		}
	}
}

// Hàm chuyển từ hệ nhị phân sang thập lục phân
string QInt::BinToHex(string Str)
{
	string Hex;
	for (int i = 127; i >= 0; i = i - 4)
	{
		int H = 0;
		if (i >= 0 && Str[i] == '1')
			H += 1;
		if (i - 1 >= 0 && Str[i - 1] == '1')
			H += 2;
		if (i - 2 >= 0 && Str[i - 2] == '1')
			H += 4;
		if (i - 3 >= 0 && Str[i - 3] == '1')
			H += 8;
		if (H < 10)
			Hex.push_back(H + 48);
		else
		{
			switch (H)
			{
			case 10:
				Hex.push_back('A');
				break;
			case 11:
				Hex.push_back('B');
				break;
			case 12:
				Hex.push_back('C');
				break;
			case 13:
				Hex.push_back('D');
				break;
			case 14:
				Hex.push_back('E');
				break;
			case 15:
				Hex.push_back('F');
				break;
			}
		}
	}
	for (int i = 0; i < Hex.size() / 2; i++)
		swap(Hex[i], Hex[Hex.size() - 1 - i]);
	return Hex;
}

// Hàm chuyển từ hệ thập phân sang thập lục phân
string QInt::DecToHex()
{
	string Bin = DecToBin();
	string Hex = BinToHex(Bin);
	return Hex;
}

// Toán tử cộng
QInt QInt::operator+(const QInt &b)
{
	QInt S;
	int Nho = 0;
	for (int i = 127; i >= 0; i--)
	{
		if (i < 32)
		{
			int bita = (data[0] >> (31 - i)) & 1;
			int bitb = (b.data[0] >> (31 - i)) & 1;
			int bit = bita + bitb + Nho;
			if (bit == 2)
				bit = 0;
			else if (bit == 3)
				bit = 1;
			S.data[0] = S.data[0] | (bit << (31 - i));
			if (bita == 1 && bitb == 1 || bita == 1 && Nho == 1 || Nho == 1 && bitb == 1)
				Nho = 1;
			else 
				Nho = 0;
		}
		else if (i < 64)
		{
			int bita = (data[1] >> (63 - i)) & 1;
			int bitb = (b.data[1] >> (63 - i)) & 1;
			int bit = bita + bitb + Nho;
			if (bit == 2)
				bit = 0;
			else if (bit == 3)
				bit = 1;
			S.data[1] = S.data[1] | (bit << (63 - i));
			if (bita == 1 && bitb == 1 || bita == 1 && Nho == 1 || Nho == 1 && bitb == 1)
				Nho = 1;
			else 
				Nho = 0;
		}
		else if (i < 96)
		{
			int bita = (data[2] >> (95 - i)) & 1;
			int bitb = (b.data[2] >> (95 - i)) & 1;
			int bit = bita + bitb + Nho;
			if (bit == 2)
				bit = 0;
			else if (bit == 3)
				bit = 1;
			S.data[2] = S.data[2] | (bit << (95 - i));
			if (bita == 1 && bitb == 1 || bita == 1 && Nho == 1 || Nho == 1 && bitb == 1)
				Nho = 1;
			else 
				Nho = 0;
		}
		else 
		{
			int bita = (data[3] >> (127 - i)) & 1;
			int bitb = (b.data[3] >> (127 - i)) & 1;
			int bit = bita + bitb + Nho;
			if (bit == 2)
				bit = 0;
			else if (bit == 3)
				bit = 1;
			S.data[3] = S.data[3] | (bit << (127 - i));
			if (bita == 1 && bitb == 1 || bita == 1 && Nho == 1 || Nho == 1 && bitb == 1)
				Nho = 1;
			else 
				Nho = 0;
		}
	}
	return S;
}

// Toán tử trừ
QInt QInt::operator-(const QInt &b)
{
	QInt S;
	QInt B;
	S.data[0] = data[0];
	S.data[1] = data[1];
	S.data[2] = data[2];
	S.data[3] = data[3];

	B = b;
	B.ToNegative();
	S = S + B;
	return S;
}

// Toán tử nhân
QInt QInt::operator*(const QInt &b)
{
	QInt S;
	QInt T;
	QInt B;

	bool KhacDau = false;
	// Chép bit vào biến tạm để thao tác
	B = b;

	T.data[0] = data[0];
	T.data[1] = data[1];
	T.data[2] = data[2];
	T.data[3] = data[3];

	// Nếu là số âm thì đổi lại thành dương
	// Ghi lại 2 số cùng dấu hay khác dấu
	if (T.data[0] >> 31 == -1 && B.data[0] >> 31 == -1)
	{
		T.ToNegative();
		B.ToNegative();
	}
	else if (T.data[0] >> 31 == -1)
	{
		T.ToNegative();
		KhacDau = true;
	}
	else if (B.data[0] >> 31 == -1)
	{
		B.ToNegative();
		KhacDau = true;
	}

	for (int i = 127; i >= 0; i--)
	{
		if (i < 32)
		{
			int bitb = (B.data[0] >> (31 - i)) & 1;
			// Lấy bit cuối bên trái nhớ sang data[i] khi dịch trái
			int bit1 = (T.data[1] >> 31) & 1;
			int bit2 = (T.data[2] >> 31) & 1;
			int bit3 = (T.data[3] >> 31) & 1;

			// Tính tổng các lần nhân
			if (bitb)
				S = S + T;

			// Dịch bit
			T.data[0] = (T.data[0] << 1) | bit1;
			T.data[1] = (T.data[1] << 1) | bit2;
			T.data[2] = (T.data[2] << 1) | bit3;
			T.data[3] = (T.data[3] << 1);

		}

		else if (i < 64)
		{
			int bitb = (B.data[1] >> (63 - i)) & 1;
			// Lấy bit cuối bên trái nhớ sang data[i] khi dịch trái
			int bit1 = T.data[1] >> 31;
			int bit2 = T.data[2] >> 31;
			int bit3 = T.data[3] >> 31;

			// Tính tổng các lần nhân
			if (bitb)
				S = S + T;

			// Dịch bit
			T.data[0] = (T.data[0] << 1) | bit1;
			T.data[1] = (T.data[1] << 1) | bit2;
			T.data[2] = (T.data[2] << 1) | bit3;
			T.data[3] = (T.data[3] << 1);

		}
		else if (i < 96)
		{
			int bitb = (B.data[2] >> (95 - i)) & 1;
			// Lấy bit cuối bên trái nhớ sang data[i] khi dịch trái
			int bit1 = T.data[1] >> 31;
			int bit2 = T.data[2] >> 31;
			int bit3 = T.data[3] >> 31;

			// Tính tổng các lần nhân
			if (bitb)
				S = S + T;

			// Dịch bit
			T.data[0] = (T.data[0] << 1) | bit1;
			T.data[1] = (T.data[1] << 1) | bit2;
			T.data[2] = (T.data[2] << 1) | bit3;
			T.data[3] = (T.data[3] << 1);

		}
		else
		{
			int bitb = (B.data[3] >> (127 - i)) & 1;
			// Lấy bit cuối bên trái nhớ sang data[i] khi dịch trái
			int bit1 = T.data[1] >> 31;
			int bit2 = T.data[2] >> 31;
			int bit3 = T.data[3] >> 31;

			// Tính tổng các lần nhân
			if (bitb)
				S = S + T;

			// Dịch bit
			T.data[0] = (T.data[0] << 1) | bit1;
			T.data[1] = (T.data[1] << 1) | bit2;
			T.data[2] = (T.data[2] << 1) | bit3;
			T.data[3] = (T.data[3] << 1);

		}
	}

	// Đổi dấu
	if (KhacDau)
		S.ToNegative();
	return S;
}

// Toán tử chia
QInt QInt::operator/(const QInt &b)
{
	QInt Q;
	QInt M;
	QInt A;

	Q.data[0] = data[0];
	Q.data[1] = data[1];
	Q.data[2] = data[2];
	Q.data[3] = data[3];

	M = b;

	bool KhacDau = false;

	// Nếu là số âm thì đổi lại thành dương
	// Ghi lại 2 số cùng dấu hay khác dấu
	if (Q.data[0] >> 31 == -1 && M.data[0] >> 31 == -1)
	{
		Q.ToNegative();
		M.ToNegative();
	}
	else if (Q.data[0] >> 31 == -1)
	{
		Q.ToNegative();
		KhacDau = true;
	}
	else if (M.data[0] >> 31 == -1)
	{
		M.ToNegative();
		KhacDau = true;
	}


	for (int i = 128; i > 0; i--)
	{
		int bit = Q.data[0] >> 31 & 1;
		Q.data[0] = (Q.data[0] << 1) | ((Q.data[1] >> 31) & 1);
		Q.data[1] = (Q.data[1] << 1) | ((Q.data[2] >> 31) & 1);
		Q.data[2] = (Q.data[2] << 1) | ((Q.data[3] >> 31) & 1);
		Q.data[3] = Q.data[3] << 1;
		A.data[0] = (A.data[0] << 1) | ((A.data[1] >> 31) & 1);
		A.data[1] = (A.data[1] << 1) | ((A.data[2] >> 31) & 1);
		A.data[2] = (A.data[2] << 1) | ((A.data[3] >> 31) & 1);	
		A.data[3]  = (A.data[3] << 1) | bit;
		A = A - M;
		if (A.data[0] >> 31 == -1)
		{
			Q.data[3] = (Q.data[3] >> 1) ;
			Q.data[3] = (Q.data[3] << 1);
			A = A + M;
		}
		else
		{
			Q.data[3] = Q.data[3] | 1;
		}
	}
	// Đổi dấu
	if (KhacDau)
		Q.ToNegative();
	return Q;
}

// Hàm reset về 0
void QInt::Reset()
{
	data[0] = data[1] = data[2] = data[3] = 0;
}
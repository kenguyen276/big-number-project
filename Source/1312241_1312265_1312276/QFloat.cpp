﻿#include "QFloat.h"
#include "QInt.h"

// Phương thức khởi tạo mặc định
QFloat::QFloat(void)
{
	data[0] = data[1] = data[2] = data[3] = 0;
}

// Phương thức hủy mặc định
QFloat::~QFloat(void)
{
	return;
}

// Hàm Chuyển phần thập phân sang nhị phân
string QFloat::BinThapPhan(string Str)
{
	int l1, l2;
	string Bin;
	for (int i = 0; i < 112; i++)
	{
		l1 = Str.length();
		Str = StrMult2(Str);
		l2 = Str.length();
		if (l2 > l1)
		{
			Str.erase(0,1);
			Bin.push_back('1');
		}
		else
			Bin.push_back('0');
	}
	return Bin;
}

// Hàm chuyển phần nguyên sang nhị phân
string QFloat::BinNguyen(string Str)
{
	string Bin;
	int bit;
	while (Str != "")
	{
		bit = (Str[Str.length() - 1] - 48) % 2;
		Bin.insert(0, 1, bit + 48);

		string Thuong;
		int Du = 0;
		for (int j = 0; j < Str.length(); j++)
		{
			int GiaTri = (Str[j] - 48) + 10 * Du;
			Du = GiaTri % 2;
			if (GiaTri < 2)
			{
				if (j != 0)
					Thuong.push_back('0');
				continue;
			}
			int T = GiaTri / 2;
			Thuong.push_back(T + 48);
		}
		Str = Thuong;
	}
	return Bin;
}

// Hàm nhập giá trị
void QFloat::Scan(string Str) 
{
	string Dau, PhanNguyen, PhanThapPhan;

	int i, LaChamDong;

	// Định dạng lại chuỗi về dạng số chấm động
	for (i = 0, LaChamDong = 0; i < Str.length(); i++)
	{
		if (Str[i] == '.')
		{
			LaChamDong = 1; 
			break;
		}
	}

	// Nếu nhập vào chuỗi số nguyên
	if (LaChamDong == 0) 
	{
		Str.push_back('.');
		Str.push_back('0');
	}

	// Xác định dấu
	Dau = "0";
	if (Str[0] == '-')
	{
		Dau = "1";
		Str[0] = '0';
	}

	// Lấy phần nguyên
	for (i = 0; Str[i] != '.'; i++)
		PhanNguyen.push_back(Str[i]);

	// Lấy phần thập phân
	for (i = i + 1; i < Str.length(); i++)
		PhanThapPhan.push_back(Str[i]);

	// Chuyển phần nguyên sang nhị phân
	PhanNguyen = BinNguyen(PhanNguyen);

	// Chuyển phần thập phân sang nhị phân
	PhanThapPhan = BinThapPhan(PhanThapPhan);

	// Kiểm tra số nhập vào có phải là số 0 hay không
	bool IsZero = true;
	for (i = 0; i < PhanNguyen.length(); i++)
	{
		if (PhanNguyen[i] == '1')
			IsZero = false;
	}

	for (i = 0; i < PhanThapPhan.length(); i++)
	{
		if (!IsZero)
			continue;
		if (PhanThapPhan[i] == '1')
			IsZero = false;
	}

	// Nếu là số 0 gán 0 cho tất cả các bit sau đó dừng chương trình
	if (IsZero)
	{
		data[0] = 0;
		data[1] = 0;
		data[2] = 0;
		data[3] = 0;
		return;
	}

	// Xác định vị trí dấu chấm động và số mũ
	int pos = -1;
	int SoMu = 0;

	// Tìm vị trí dấu chấm động trên phần nguyên
	for (int i = PhanNguyen.length() - 1; i >= 0; i--)
		if (PhanNguyen[i] == '1')
			pos = i;

	// Nếu số nhập vào có giá trị >= 1 thì dấu chấm động nằm trên phần nguyên
	if (pos >= 0) 
	{
		// Dịch từ phần nguyên sang phần thập phân
		for (int i = PhanNguyen.length() - 1; i > pos; i--)
		{
			if (PhanNguyen[i] == '0')
				PhanThapPhan.insert(0, "0");
			else
				PhanThapPhan.insert(0, "1");
			PhanNguyen.pop_back();

			// Tăng số mũ
			SoMu++;
		}
	} 
	// Nếu số nhỏ hơn 1 dấu chấm động nằm trên phần thập phân
	else 
	{
		// Tìm vị trí bit 1 đầu tiên trong phần thập phân
		for (int i = 0; i < PhanThapPhan.length(); i++)
			if (PhanThapPhan[i] == '1')
			{
				pos = i;
				break;
			}

			// Dịch từ phần thập phân sang phần nguyên
			for (int i = 0; i <= pos; i++)
			{
				if (PhanThapPhan[0] == '1')
					PhanNguyen.push_back('1');
				PhanThapPhan.erase(0, 1);
				// Giảm số mũ
				SoMu--;
			}
	}

	// Phần thập phân 112 bit
	if (PhanThapPhan.length() > 112)
		PhanThapPhan.resize(112);
	else if (PhanThapPhan.length() < 112)
	{
		int k = PhanThapPhan.length();
		for (int i = 0; i < 112 - k; i++)
			PhanThapPhan.push_back('0');
	}

	// Số mũ + số Bias
	SoMu += 16383;

	string KQ; // Chuỗi biểu diễn số chấm động dưới dạng nhị phân

	// Dấu
	KQ.push_back(Dau[0]);

	// Mũ
	for (int i = 0, bit; i < 15; i++)
	{
		bit = ( SoMu >> (14 - i) & 1);
		KQ.push_back(bit + 48);
	}

	// Phần thập phân
	for (int i = 0; i < 112; i++)
		KQ.push_back(PhanThapPhan[i]);

	//Thực hiện gán bit vào QFloat từ chuỗi
	for (int i = 127; i >= 0; i--)
	{
		// Lấy bit
		int bit = KQ[i] - 48;

		// Gán bit vào data[0]
		if (i < 32)
		{
			bit = bit << (31 - i);
			data[0] = data[0] | bit;
		}
		// Gán bit vào data[1]
		else if (i < 64)
		{
			bit = bit << (63 - i);
			data[1] = data[1] | bit;
		}
		// Gán bit vào data[2]
		else if (i < 96)
		{
			bit = bit << (95 - i);
			data[2] = data[2] | bit;
		}
		// Gán bit vào data[3]
		else
		{
			bit = bit << (127 - i);
			data[3] = data[3] | bit;
		}
	}
}

// Hàm xuất giá trị
string QFloat::Print() 
{
	string Dau, Nguyen, ThapPhan, GiaTri;
	int SoMu;

	// Kiểm tra trường hợp là số 0
	if (data[0] == 0 && data[1] == 0 && data[2] == 0 && data[3] == 0)
		return "0.0";

	// Xác định dấu
	if ( (data[0] >> 31) & 1 == 1 )
		GiaTri.push_back('-');

	// Các thao tác xác định số mũ
	SoMu = 0;
	for (int i = 16; i < 31; i++)
	{
		int bit = (data[0] >> i) & 1;
		bit = bit << (i - 16);
		SoMu = SoMu | bit;
	}

	SoMu -= 16383;

	// Lấy tất cả các phần còn lại vào thập phân sau sẽ tách riêng
	for (int i = 16; i < 128; i++)
	{
		int bit;

		if (i < 32)
			bit = data[0] >> (31 - i) & 1;
		else if (i < 64)
			bit = data[1] >> (63 - i) & 1;
		else if (i < 96)
			bit = data[2] >> (95 - i) & 1;
		else
			bit = data[3] >> (127 - i) & 1;

		ThapPhan.push_back(bit + 48);
	}

	// Các thao tác thực hiện tách riêng phần nguyên và phần thập phân
	if (SoMu >= 0)
	{
		Nguyen.push_back('1');
		while (SoMu > 0 && ThapPhan.length() > 0)
		{
			Nguyen.push_back(ThapPhan[0]);
			ThapPhan.erase(0, 1);
			SoMu--;
		}

	}

	// Nếu mũ âm
	else if (SoMu < 0)
	{
		Nguyen.push_back('0');
		ThapPhan.insert(0, 1, '1');
		SoMu++;
		while (SoMu < 0)
		{
			ThapPhan.insert(0, 1, '0');
			SoMu++;
		}
	}

	// Tính  giá trị phần nguyên
	Nguyen = Bin2Dec(Nguyen);
	for (int i = 0; i < Nguyen.length(); i++)
		GiaTri.push_back(Nguyen[i]);

	// Tính giá trị phần thập phân
	string StrMax = "1";
	string ThPhan = "0";

	for (int i = 0; i < ThapPhan.length(); i++)
	{
		StrMax.push_back('0');
		if (ThapPhan[i] == '1')
		{
			string temp;
			string Str = StrMax;
			for (int k = 0; k < i + 1; k++)
			{
				int Du = 0;
				for (int j = 0; j < Str.length(); j++)
				{
					int GiaTri = (Str[j] - 48) + 10 * Du;
					Du = GiaTri % 2;
					if (GiaTri < 2)
					{
						if (j != 0)
							temp.push_back('0');
						continue;
					}
					int T = GiaTri / 2;
					temp.push_back(T + 48);
				}
				Str = temp;
				temp.clear();
			}
			ThPhan = StrAddStr(ThPhan, Str);
		}
		if (ThPhan != "0")
		{
			ThPhan.push_back('0');
			while (ThPhan.length() < StrMax.length())
				ThPhan.insert(0, 1, '0');
		}
	}

	while(ThPhan[ThPhan.length() - 1] == '0' && ThPhan.length() > 2)
		ThPhan.pop_back();

	// Chuỗi xuát màn hình ở dạng "phần nguyên" + "." + "phần thập phân"
	GiaTri.push_back('.'); 
	GiaTri = GiaTri + ThPhan;

	if (ThapPhan.length() == 0)
		GiaTri.push_back('0');
	
	return GiaTri;
}

// Hàm chuyển từ thập phân sang nhị phân
string QFloat::DecToBin() 
{
	int bit;
	string Bin;
	// Đọc bit ra từ các data
	for (int i = 0; i < 128; i++)
	{
		if (i < 32)
			bit = data[0] >> (31 - i) & 1;
		else if (i < 64)
			bit = data[1] >> (63 - i) & 1;
		else if (i < 96)
			bit = data[2] >> (95 - i) & 1;
		else
			bit = data[3] >> (127 - i) & 1;

		Bin.push_back(bit + 48);
		if (i == 0 || i == 15)
			Bin.push_back(' ');
	}
	return Bin;
}

// Hàm chuyển từ nhị phân sang thập phân
string QFloat::BinToDec(string Bin) 
{
	// Gán bit vào các data
	for (int i = 127; i >= 0; i--)
	{
		// Lấy bit
		int bit = Bin[i] - 48;

		// Gán bit vào data[0]
		if (i < 32)
		{
			bit = bit << (31 - i);
			data[0] = data[0] | bit;
		}
		// Gán bit vào data[1]
		else if (i < 64)
		{
			bit = bit << (63 - i);
			data[1] = data[1] | bit;
		}
		// Gán bit vào data[2]
		else if (i < 96)
		{
			bit = bit << (95 - i);
			data[2] = data[2] | bit;
		}
		// Gán bit vào data[3]
		else
		{
			bit = bit << (127 - i);
			data[3] = data[3] | bit;
		}
	}
	return this->Print();
}

// Toán tử cộng
QFloat QFloat::operator+(QFloat b) 
{
	QFloat A, B;
	A = *this;
	B = b;

	// Nếu có một trong 2 số bằng 0 thì trả về số còn lại
	if (data[0] == 0 && data[1] == 0 && data[2] == 0 && data[3] == 0)
		return b;

	if (b.data[0] == 0 && b.data[1] == 0 && b.data[2] == 0 && b.data[3] == 0)
		return *this;

	// Hai số đều khác 0
	// Xác định dấu
	int Dau;
	int Dau1 = (data[0] >> 31) & 1;
	int Dau2 = (b.data[0] >> 31) & 1;

	if (Dau1 == Dau2 && Dau2 == 0)
		// Dấu của tổng hai số dương là dương
		Dau = 0;
	else if (Dau1 == Dau2 && Dau2 == 1)
		// Dấu của tổng hai số âm là âm
		Dau = 1;
	else
		// Dấu của tổng hai số khác dấu là dấu của số lớn hơn
		Dau = -1;

	QFloat c;
	// Cộng số cùng dấu
	if (Dau == 0 || Dau == 1) 
	{
		int Mu1, Mu2;
		Mu1 = 0;
		Mu2 = 0;

		// Tính số mũ
		for (int i = 16; i < 31; i++)
		{
			int bit1 = (data[0] >> i) & 1;
			int bit2 = (b.data[0] >> i) & 1;
			Mu1 = Mu1 | (bit1 << (i - 16));
			Mu2 = Mu2 | (bit2 << (i - 16));
		}


		int SoMuTang = 0;
		if (Mu1 == Mu2)
		{
			SoMuTang = 1;
		}

		if (Mu1 > Mu2)
		{
			QFloat temp;
			temp.data[0] = b.data[0];
			temp.data[1] = b.data[1];
			temp.data[2] = b.data[2];
			temp.data[3] = b.data[3];

			b.data[0] = data[0];
			b.data[1] = data[1];
			b.data[2] = data[2];
			b.data[3] = data[3];

			data[0] = temp.data[0];
			data[1] = temp.data[1];
			data[2] = temp.data[2];
			data[3] = temp.data[3];

			swap(Mu2, Mu1);
		}

		for (int i = 0; Mu1 < Mu2; i++)
		{
			// Tăng mũ
			Mu1++;

			// Dịch bit sang phải
			int bit01 = data[0] & 1;
			int bit12 = data[1] & 1;
			int bit23 = data[2] & 1;

			int temp = 0, j = 0, bit;

			// Dịch phải data[3]
			data[3] = data[3] >> 1;
			for (j = 0; j < 31; j++)
			{
				bit = (data[3] >> j) & 1;
				temp = temp | (bit << j);
			}
			temp = temp | (bit23 << 31);
			data[3] = temp;

			// Dịch phải data[2]
			temp = 0;
			data[2] = data[2] >> 1;
			for (j = 0; j < 31; j++)
			{
				bit = (data[2] >> j) & 1;
				temp = temp | (bit << j);
			}
			temp = temp | (bit12 << 31);
			data[2] = temp;

			// Dịch phải data[1]
			temp = 0;
			data[1] = data[1] >> 1;
			for (j = 0; j < 31; j++)
			{
				bit = (data[1] >> j) & 1;
				temp = temp | (bit << j);
			}
			temp = temp | (bit01 << 31);
			data[1] = temp;


			temp = 0;
			data[0] = data[0] >> 1;
			for (j = 0; j < 15; j++)
			{
				bit = (data[0] >> j) & 1;
				temp = temp | (bit << j);
			}

			if (i == 0)
				bit = 1;
			else
				bit = 0;

			temp = temp | (bit << j);

			// Nếu là số dương
			if (((data[0] >> 31) & 1) == 0)
				data[0] = Mu1 << 16;
			else
			{
				data[0] = Mu1 << 16;
				data[0] = data[0] | (1 << 31);
			}

			data[0] = data[0] | temp;
		}
		
		// Cộng phần trị
		
		int Nho = 0;
		for (int i = 127; i >= 16; i--)
		{
			if (i < 32)
			{
				int bita = (data[0] >> (31 - i)) & 1;
				int bitb = (b.data[0] >> (31 - i)) & 1;
				int bit = bita + bitb + Nho;
				if (bit == 2)
					bit = 0;
				else if (bit == 3)
					bit = 1;
				c.data[0] = c.data[0] | (bit << (31 - i));
				if (bita == 1 && bitb == 1 || bita == 1 && Nho == 1 || Nho == 1 && bitb == 1)
					Nho = 1;
				else
					Nho = 0;
			}
			else if (i < 64)
			{
				int bita = (data[1] >> (63 - i)) & 1;
				int bitb = (b.data[1] >> (63 - i)) & 1;
				int bit = bita + bitb + Nho;
				if (bit == 2)
					bit = 0;
				else if (bit == 3)
					bit = 1;
				c.data[1] = c.data[1] | (bit << (63 - i));
				if (bita == 1 && bitb == 1 || bita == 1 && Nho == 1 || Nho == 1 && bitb == 1)
					Nho = 1;
				else
					Nho = 0;
			}
			else if (i < 96)
			{
				int bita = (data[2] >> (95 - i)) & 1;
				int bitb = (b.data[2] >> (95 - i)) & 1;
				int bit = bita + bitb + Nho;
				if (bit == 2)
					bit = 0;
				else if (bit == 3)
					bit = 1;
				c.data[2] = c.data[2] | (bit << (95 - i));
				if (bita == 1 && bitb == 1 || bita == 1 && Nho == 1 || Nho == 1 && bitb == 1)
					Nho = 1;
				else
					Nho = 0;
			}
			else
			{
				int bita = (data[3] >> (127 - i)) & 1;
				int bitb = (b.data[3] >> (127 - i)) & 1;
				int bit = bita + bitb + Nho;
				if (bit == 2)
					bit = 0;
				else if (bit == 3)
					bit = 1;
				c.data[3] = c.data[3] | (bit << (127 - i));
				if (bita == 1 && bitb == 1 || bita == 1 && Nho == 1 || Nho == 1 && bitb == 1)
					Nho = 1;
				else
					Nho = 0;
			}
		}

		for (int i = 16; i < 31; i++)
		{
			int bit = (data[0] >> i) & 1;
			c.data[0] = c.data[0] | (bit << i);
		}

		// Nếu phần trị bị tràn
		if (Nho == 1)
		{
			// Tăng mũ
			Mu1++;

			// Dịch bit sang phải
			int bit01 = data[0] & 1;
			int bit12 = data[1] & 1;
			int bit23 = data[2] & 1;

			int temp = 0, j = 0, bit;

			// Dịch phải data[3]
			data[3] = data[3] >> 1;
			for (j = 0; j < 31; j++)
			{
				bit = (data[3] >> j) & 1;
				temp = temp | (bit << j);
			}
			temp = temp | (bit23 << 31);
			data[3] = temp;

			// Dịch phải data[2]
			temp = 0;
			data[2] = data[2] >> 1;
			for (j = 0; j < 31; j++)
			{
				bit = (data[2] >> j) & 1;
				temp = temp | (bit << j);
			}
			temp = temp | (bit12 << 31);
			data[2] = temp;

			// Dịch phải data[1]
			temp = 0;
			data[1] = data[1] >> 1;
			for (j = 0; j < 31; j++)
			{
				bit = (data[1] >> j) & 1;
				temp = temp | (bit << j);
			}
			temp = temp | (bit01 << 31);
			data[1] = temp;


			temp = 0;
			c.data[0] = c.data[0] >> 1;

			for (j = 0; j < 15; j++)
			{
				bit = (c.data[0] >> j) & 1;
				temp = temp | (bit << j);
			}

			if (SoMuTang == 1)
			{
				bit = 1;
				SoMuTang = 0;
			}
			else
				bit = 0;

			temp = temp | (bit << j);

			// Dấu
			if (((c.data[0] >> 31) & 1) == 0)
				c.data[0] = Mu1 << 16;
			else
			{
				c.data[0] = Mu1 << 16;
				c.data[0] = c.data[0] | (1 << 31);
			}

			c.data[0] = c.data[0] | temp;
		}

		if (SoMuTang == 1)
		{
			// Tăng mũ
			Mu1++;

			// Dịch bit sang phải
			int bit01 = c.data[0] & 1;
			int bit12 = c.data[1] & 1;
			int bit23 = c.data[2] & 1;

			c.data[3] = (c.data[3] >> 1) | (bit23 << 31);
			c.data[2] = (c.data[2] >> 1) | (bit12 << 31);
			c.data[1] = (c.data[1] >> 1) | (bit01 << 31);

			int temp = 0;
			int bit, j;

			c.data[0] = c.data[0] >> 1;

			for (j = 0; j < 15; j++)
			{
				bit = (c.data[0] >> j) & 1;
				temp = temp | (bit << j);
			}

			bit = 0;
			temp = temp | (bit << j);

			// Dấu
			if (((c.data[0] >> 31) & 1) == 0)
				c.data[0] = Mu1 << 16;
			else
			{
				c.data[0] = Mu1 << 16;
				c.data[0] = c.data[0] | (1 << 31);
			}

			c.data[0] = c.data[0] | temp;
		}

		// Dấu
		if (Dau == 1)
			c.data[0] = c.data[0] | (1 << 31);
		*this = A;
		b = B;
		return c;
	}
	// Cộng số khác dấu
	else  
	{
		// Kiểm tra 2 số có cùng 1 giá trị không 
		if (data[1] == b.data[1] && data[2] == b.data[2] && data[3] == b.data[3])
		{

			int tdata = 0;
			if (data[0] > 0)
			{
				tdata = data[0] | (1 << 31);
				if (tdata == b.data[0])
				return c;
			}
			else if (b.data[0] > 0)
			{
				tdata = b.data[0] | (1 << 31);
				if (tdata == data[0])
					return c;
			}
		}

		// Tìm số có trị tuyệt đối lớn hơn và gán dữ liệu của số đó là data
		for (int i = 1; i < 127; i++)
		{
			int bit1, bit2;
			if (i < 32)
			{
				bit1 = data[0] >> (31 - i) & 1;
				bit2 = b.data[0] >> (31 - i) & 1;
				if (bit1 < bit2)
				{
					QFloat temp;
					temp.data[0] = data[0];
					temp.data[1] = data[1];
					temp.data[2] = data[2];
					temp.data[3] = data[3];

					data[0] = b.data[0];
					data[1] = b.data[1];
					data[2] = b.data[2];
					data[3] = b.data[3];

					b = temp;
					break;
				}
				else if (bit1 > bit2)
					break;
			}
			else if (i < 64)
			{
				bit1 = data[0] >> (63 - i) & 1;
				bit2 = b.data[0] >> (63 - i) & 1;
				if (bit1 < bit2)
				{
					QFloat temp;
					temp.data[0] = data[0];
					temp.data[1] = data[1];
					temp.data[2] = data[2];
					temp.data[3] = data[3];

					data[0] = b.data[0];
					data[1] = b.data[1];
					data[2] = b.data[2];
					data[3] = b.data[3];

					b = temp;
					break;
				}
				else if (bit1 > bit2)
					break;
			}
			else if (i < 96)
			{
				bit1 = data[0] >> (95 - i) & 1;
				bit2 = b.data[0] >> (95 - i) & 1;
				if (bit1 < bit2)
				{
					QFloat temp;
					temp.data[0] = data[0];
					temp.data[1] = data[1];
					temp.data[2] = data[2];
					temp.data[3] = data[3];

					data[0] = b.data[0];
					data[1] = b.data[1];
					data[2] = b.data[2];
					data[3] = b.data[3];

					b = temp;
					break;
				}
				else if (bit1 > bit2)
					break;
			}
			else if (i < 128)
			{
				bit1 = data[0] >> (127 - i) & 1;
				bit2 = b.data[0] >> (127 - i) & 1;
				if (bit1 < bit2)
				{
					QFloat temp;
					temp.data[0] = data[0];
					temp.data[1] = data[1];
					temp.data[2] = data[2];
					temp.data[3] = data[3];

					data[0] = b.data[0];
					data[1] = b.data[1];
					data[2] = b.data[2];
					data[3] = b.data[3];

					b = temp;
					break;
				}
				else if (bit1 > bit2)
					break;
			}
		}

		// Tính số mũ

		int Mu1 = 0, Mu2 = 0, Mu = 16382;

		for (int i = 16; i < 31; i++)
		{
			int bit1 = (data[0] >> i) & 1;
			int bit2 = (b.data[0] >> i) & 1;
			Mu1 = Mu1 | (bit1 << (i - 16));
			Mu2 = Mu2 | (bit2 << (i - 16));
		}

		int SoMuTang = 0;
		if (Mu1 == Mu2)
		{
			SoMuTang = 1;
		}

		// Kiểm tra mũ
		for (int i = 0; Mu1 > Mu2; i++)
		{
			// Tăng mũ
			Mu2++;

			// Dịch bit sang phải
			int bit01 = b.data[0] & 1;
			int bit12 = b.data[1] & 1;
			int bit23 = b.data[2] & 1;

			int temp = 0, j = 0, bit;
			b.data[3] = b.data[3] >> 1;
			for (j = 0; j < 31; j++)
			{
				bit = (b.data[3] >> j) & 1;
				temp = temp | (bit << j);
			}
			temp = temp | (bit23 << 31);
			b.data[3] = temp;

			temp = 0;
			b.data[2] = b.data[2] >> 1;
			for (j = 0; j < 31; j++)
			{
				bit = (b.data[2] >> j) & 1;
				temp = temp | (bit << j);
			}
			temp = temp | (bit12 << 31);
			b.data[2] = temp;

			temp = 0;
			b.data[1] = b.data[1] >> 1;
			for (j = 0; j < 31; j++)
			{
				bit = (b.data[1] >> j) & 1;
				temp = temp | (bit << j);
			}
			temp = temp | (bit01 << 31);
			b.data[1] = temp;

			temp = 0;
			b.data[0] = b.data[0] >> 1;
			for (j = 0; j < 15; j++)
			{
				bit = (b.data[0] >> j) & 1;
				temp = temp | (bit << j);
			}

			if (i == 0)
				bit = 1;
			else
				bit = 0;

			temp = temp | (bit << j);

			// Nếu là số dương
			if (((b.data[0] >> 31) & 1) == 0)
				b.data[0] = Mu1 << 16;
			else
			{
				b.data[0] = Mu1 << 16;
				b.data[0] = b.data[0] | (1 << 31);
			}

			b.data[0] = b.data[0] | temp;
		}

		// Thực hiện trừ bit phần trị
		int Nho = 0;
		for (int i = 127; i >= 16; i--)
		{
			if (i < 32)
			{
				int bita = (data[0] >> (31 - i)) & 1;
				int bitb = (b.data[0] >> (31 - i)) & 1;
				int bit = bita - bitb - Nho;
				if (bit == -1)
				{
					bit = 1;
					Nho = 1;
				}
				else if (bit == -2)
				{
					bit = 0;
					Nho = 1;
				}
				else
					Nho = 0;	
				c.data[0] = c.data[0] | (bit << (31 - i));
			}
			else if (i < 64)
			{
				int bita = (data[1] >> (63 - i)) & 1;
				int bitb = (b.data[1] >> (63 - i)) & 1;
				int bit = bita - bitb - Nho;
				if (bit == -1)
				{
					bit = 1;
					Nho = 1;
				}
				else if (bit == -2)
				{
					bit = 0;
					Nho = 1;
				}
				else
					Nho = 0;	
				c.data[1] = c.data[1] | (bit << (63 - i));
			}
			else if (i < 96)
			{
				int bita = (data[2] >> (95 - i)) & 1;
				int bitb = (b.data[2] >> (95 - i)) & 1;
				int bit = bita - bitb - Nho;
				if (bit == -1)
				{
					bit = 1;
					Nho = 1;
				}
				else if (bit == -2)
				{
					bit = 0;
					Nho = 1;
				}
				else
					Nho = 0;
				Nho = 0;	
				c.data[2] = c.data[2] | (bit << (95 - i));
			}
			else 
			{
				int bita = (data[3] >> (127 - i)) & 1;
				int bitb = (b.data[3] >> (127 - i)) & 1;
				int bit = bita - bitb - Nho;
				if (bit == -1)
				{
					bit = 1;
					Nho = 1;
				}
				else if (bit == -2)
				{
					bit = 0;
					Nho = 1;
				}
				else 
					Nho = 0;	
				c.data[3] = c.data[3] | (bit << (127 - i));
			}
		}
	
		// Phần lập trình bị lỗi
		if (Nho == 1 || SoMuTang == 1)
		{
			int count = 0;
			for (int i = 15; i >= 0; i--)
			{
				int bit = c.data[0] >> i & 1;
				count++;
				if (bit)
					break;
			}

			for (int i = 0; i < count; i++)
			{
				int bit32 = c.data[3] >> 31 & 1;
				int bit21 = c.data[2] >> 31 & 1;
				int bit10 = c.data[1] >> 31 & 1;

				c.data[3] = (c.data[3] << 1); 
				c.data[2] = (c.data[2] << 1) | bit32;
				c.data[1] = (c.data[1] << 1) | bit21;
				
				int temp = 0;
				temp = temp | bit10;
				for (int i = 0; i < 15; i++)
				{
					int bit = c.data[0] >> i & 1;
					temp = temp | (bit << i + 1);
				}
				c.data[0] = temp;
				Mu1--;
			}
		}

		for (int i = 16; i < 31; i++)
		{
			int bit = (Mu1 >> (i - 16)) & 1;
			c.data[0] = c.data[0] | (bit << i);
		}
		// Dấu kết quả là dấu của số có trị tuyệt đối lớn hơn
		c.data[0] = c.data[0] | ((data[0] >> 31) << 31);

		*this = A;
		b = B;
		return c;
	}
}

// Toán tử trừ
QFloat QFloat::operator-(QFloat b) 
{
	// Đổi dấu của b
	int tmpdata = 0;
	for (int i = 0; i < 31; i++)
	{
		int bit = (b.data[0] >> i) & 1;
		tmpdata = tmpdata | (bit << i);
	}
	tmpdata = tmpdata | (~((b.data[0] >> 31) & 1) << 31);
	b.data[0] = tmpdata;
	// a - b = a + (-b);
	return *this + b;
}

// Toán tử nhân
QFloat QFloat::operator*(QFloat b) 
{
	// Kiểm tra xem có số nào bằng không hay không. Nếu có trả về kết quả là không
	if (data[0] == 0 && data[1] == 0 && data[2] == 0 && data[3] == 0)
		return *this;

	if (b.data[0] == 0 && b.data[1] == 0 && b.data[2] == 0 && b.data[3] == 0)
		return b;

	// Tính số mũ
	int Mu1 = 0, Mu2 = 0, Mu = 0;

	for (int i = 16; i < 31; i++)
	{
		int bit1 = (data[0] >> i) & 1;
		int bit2 = (b.data[0] >> i) & 1;
		Mu1 = Mu1 | (bit1 << (i - 16));
		Mu2 = Mu2 | (bit2 << (i - 16));
	}

	// Biến kết quả phép nhân
	QFloat c;

	// Mũ tích bằng mũ số thứ nhất + mũ số thứ 2 - Bias
	Mu = Mu1 + Mu2 - 16383;

	// Xác định dấu của tích
	int Dau;
	int Dau1 = (data[0] >> 31) & 1;
	int Dau2 = (b.data[0] >> 31) & 1;

	// Nhân hai số cùng dấu ta được tích có dấu dương, ngược lại cho dấu âm
	if ((Dau1 == Dau2 && Dau2 == 0) || (Dau1 == Dau2 && Dau2 == 1))
		Dau = 0;
	else
		Dau = 1;
	// Gán dấu cho tích
	c.data[0] = (Dau << 31);

	// Thực hiện nhân phần trị

	// Phần nội dung chưa hoàn thành
	c.data[0] = c.data[1] = c.data[2] = c.data[3] = 0;


	return c;
}

// Toán tử chia
QFloat QFloat::operator/(QFloat b){
	// Biến kết quả phép chia
	QFloat c;

	// Kiểm tra nếu số chia bằng 0 thì trả về kết quả là 0
	if (data[0] == 0 && data[1] == 0 && data[2] == 0 && data[3] == 0)
		return *this;

	// Nếu số bị chia bằng 0 trả về -1
	if (b.data[0] == 0 && b.data[1] == 0 && b.data[2] == 0 && b.data[3] == 0)
	{
		c.Scan("-1.0");
		return c;
	}

	// Tính số mũ
	int Mu1 = 0, Mu2 = 0, Mu = 0;

	for (int i = 16; i < 31; i++)
	{
		int bit1 = (data[0] >> i) & 1;
		int bit2 = (b.data[0] >> i) & 1;
		Mu1 = Mu1 | (bit1 << (i - 16));
		Mu2 = Mu2 | (bit2 << (i - 16));
	}


	// Mũ thương bằng mũ số thứ nhất - mũ số thứ 2 + Bias
	Mu = Mu1 - Mu2 + 16383;

	// Xác định dấu của thương
	int Dau;
	int Dau1 = (data[0] >> 31) & 1;
	int Dau2 = (b.data[0] >> 31) & 1;

	// Chia hai số cùng dấu ta được tích có dấu dương, ngược lại cho dấu âm
	if ((Dau1 == Dau2 && Dau2 == 0) || (Dau1 == Dau2 && Dau2 == 1))
		Dau = 0;
	else
		Dau = 1;
	// Gán dấu cho thương
	c.data[0] = (Dau << 31);

	// Thực hiện chia phần trị

	// Phần nội dung chưa hoàn thành
	c.data[0] = c.data[1] = c.data[2] = c.data[3] = 0;
	return c;
} 


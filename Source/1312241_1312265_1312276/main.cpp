﻿#include "DInt.h"
#include "QInt.h"
#include "QFloat.h"


void main()
{
	int Chon = 0;
	while (Chon != 4)
	{
		cout << "\n--DO AN 1 - BIEU DIEN SO HOC TREN MAY TINH--\n\n";
		cout << "Chon kieu du lieu:\n";
		cout << "  1. DInt\n";
		cout << "  2. QInt\n";
		cout << "  3. QFloat\n";
		cout << "  4. Thoat\n";

		cout << "\nGVHD: Thay Le Viet Long\n";
		cout << "\nNhom sinh vien: \n";

		cout << "1312241 - Nguyen Anh Huy\n";
		cout << "1312265 - Diep Minh Hung\n";
		cout << "1312276 - Nguyen Phu Ke\n";

		cin >> Chon;

		if (Chon == 1) // Chọn kiểu dữ liệu DInt
		{
			system("cls");
			DInt X, Y, Z;
			string x, y;
			cout << "--DInt--\n";
			cout << "Lua chon chuc nang: \n";
			cout << "1. Chuyen doi giua cac he co so\n";
			cout << "2. Thuc hien cac phep toan\n";
			cin >> Chon;
			if (Chon == 1) // Chuyển đổi cơ số
			{
				system("cls");
				cout << "--DInt--\n";
				cout << "Chon chuc nang: \n";
				cout << "1. Dec To Bin & Dec To Hex\n";
				cout << "2. Bin To Dec & Bin To Hex\n";
				cin >> Chon;
				if (Chon == 1) // Dec to bin & to hex
				{
					system("cls");
					cout << "--DInt--\n";
					cout << "Dec: ";
					fflush(stdin);
					getline(cin, x);
					X.Scan(x);
					cout << "Dec: " << X.Print() << endl;
					cout << "Bin: " << X.DecToBin() << endl;
					cout << "Hex: " << X.DecToHex() << endl;
				}
				else if (Chon == 2) // Bin to dec & to hex
				{
					system("cls");
					cout << "--DInt--\n";
					cout << "-> Nhap du 64 bit\n";
					cout << "Bin: ";
					fflush(stdin);
					getline(cin, x);
					if (x.length() < 64)
					{
						int l = x.length();
						x.resize(128);
						for (int i = 0; i < 128 - l; i++)
							x.insert(0, "0");
					}
					X.BinToDec(x);
					cout << "Dec: " << X.Print() << endl;
					cout << "Hex: " << X.BinToHex(x) << endl;
				}
			}
			else // Thực hiện phép toán
			{
				system("cls");
				cout << "--DInt--\n";
				cout << "Cac phep toan: \n";
				cout << "1. X + Y\n";
				cout << "2. X - Y\n";
				cout << "3. X * Y\n";
				cout << "4. X / Y\n";

				cout << "\nX: ";
				fflush(stdin);
				getline(cin, x);
				X.Scan(x);

				cout << "Y: ";
				fflush(stdin);
				getline(cin, y);
				Y.Scan(y);

				Z = X + Y;
				cout << "\nX + Y = " << Z.Print() << endl;

				Z = X - Y;
				cout << "X - Y = " << Z.Print() << endl;

				Z = X * Y;
				cout << "X * Y = " << Z.Print() << endl;

				Z = X / Y;
				cout << "X / Y = " << Z.Print() << endl;
			}
		}
		else if (Chon == 2) // Chọn kiểu dữ liệu QInt
		{
			system("cls");
			QInt X, Y, Z;
			string x, y;
			cout << "--QInt--\n";
			cout << "Lua chon chuc nang: \n";
			cout << "1. Chuyen doi giua cac he co so\n";
			cout << "2. Thuc hien cac phep toan\n";
			cin >> Chon;
			if (Chon == 1) // Chuyển đổi cơ số
			{
				system("cls");
				cout << "--QInt--\n";
				cout << "Chon chuc nang: \n";
				cout << "1. Dec To Bin & Dec To Hex\n";
				cout << "2. Bin To Dec & Bin To Hex\n";
				cin >> Chon;
				if (Chon == 1) // Dec to bin & to hex
				{
					system("cls");
					cout << "--QInt--\n";
					cout << "Dec: ";
					fflush(stdin);
					getline(cin, x);
					X.Scan(x);
					cout << "Dec: " << X.Print() << endl;
					cout << "Bin: " << X.DecToBin() << endl;
					cout << "Hex: " << X.DecToHex() << endl;
				}
				else if (Chon == 2) // Bin to dec & to hex
				{
					system("cls");
					cout << "--QInt--\n";
					cout << "-> Nhap du 128 bit\n";
					cout << "Bin: ";
					fflush(stdin);
					getline(cin, x);
					if (x.length() < 128)
					{
						int l = x.length();
						x.resize(128);
						for (int i = 0; i < 128 - l; i++)
							x.insert(0, "0");
					}
					X.BinToDec(x);
					cout << "Dec: " << X.Print() << endl;
					cout << "Hex: " << X.BinToHex(x) << endl;
				}
			}
			else // Thực hiện phép toán
			{
				system("cls");
				cout << "--QInt--\n";
				cout << "Cac phep toan: \n";
				cout << "1. X + Y\n";
				cout << "2. X - Y\n";
				cout << "3. X * Y\n";
				cout << "4. X / Y\n";

				cout << "\nX: ";
				fflush(stdin);
				getline(cin, x);
				X.Scan(x);

				cout << "Y: ";
				fflush(stdin);
				getline(cin, y);
				Y.Scan(y);

				Z = X + Y;
				cout << "\nX + Y = " << Z.Print() << endl;

				Z = X - Y;
				cout << "X - Y = " << Z.Print() << endl;

				Z = X * Y;
				cout << "X * Y = " << Z.Print() << endl;

				Z = X / Y;
				cout << "X / Y = " << Z.Print() << endl;
			}
		}
		else if (Chon == 3) // Chọn kiểu dữ liệu QFloat
		{
			system("cls");
			QFloat X, Y, Z;
			string x, y;

			cout << "--QFloat--\n";
			cout << "Lua chon chuc nang: \n";
			cout << "1. Chuyen doi giua cac he co so\n";
			cout << "2. Thuc hien cac phep toan\n";

			cin >> Chon;
			if (Chon == 1) // Chuyển đổi cơ số
			{
				system("cls");
				cout << "--QFloat--\n";
				cout << "Chon chuc nang: \n";
				cout << "1. Dec To Bin\n";
				cout << "2. Bin To Dec\n";
				cin >> Chon;

				if (Chon == 1) // Dec to bin 
				{
					system("cls");
					cout << "--QFloat--\n";
					cout << "Dec: ";
					fflush(stdin);
					getline(cin, x);
					X.Scan(x);
					cout << "Dec: " << X.Print() << endl;
					cout << "Bin: " << X.DecToBin() << endl;
				}

				else if (Chon == 2) // Bin to dec
				{
					system("cls");
					cout << "--QFloat--\n";
					cout << "-> Nhap du 128 bit\n";
					cout << "Bin: ";
					fflush(stdin);
					getline(cin, x);
					cout << "Dec: " << X.BinToDec(x) << endl;
				}

			}
			else // Thực hiện phép toán
			{
				system("cls");
				cout << "--QFloat--\n";
				cout << "Cac phep toan: \n";
				cout << "1. X + Y\n";
				cout << "2. X - Y\n";
				cout << "3. X * Y\n";
				cout << "4. X / Y\n";

				cout << "\nX: ";
				fflush(stdin);
				getline(cin, x);
				X.Scan(x);

				cout << "Y: ";
				fflush(stdin);
				getline(cin, y);
				Y.Scan(y);

				Z = X + Y;
				cout << "\nX + Y = " << Z.Print() << endl;

				Z = X - Y;
				cout << "X - Y = " << Z.Print() << endl;

				Z = X * Y;
				cout << "X * Y = " << Z.Print() << endl;

				Z = X / Y;
				cout << "X / Y = " << Z.Print() << endl;
			}
		}
		else // Chọn Thoát
			exit(0);
	}

}
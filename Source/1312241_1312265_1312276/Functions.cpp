﻿#include "Header.h"
#include "DInt.h"
#include "QInt.h"
#include "QFloat.h"


// Hàm nhân một chuỗi với 2
string StrMult2(string Str)
{
	string Tich;
	int Nho = 0;
	for (int i = Str.size() - 1; i >= 0; i--)
	{
		int T = (Str[i] - 48) * 2 + Nho;
		if ( T > 9)
			Nho = 1;
		else 
			Nho = 0;
		Tich.push_back((T % 10) + 48);
	}
	if (Nho)
		Tich.push_back('1');
	for (int i = 0; i < Tich.size() / 2; i++)
		swap(Tich[i], Tich[Tich.size() - 1 - i]);
	return Tich;
}

// Hàm cộng hai chuỗi
string StrAddStr(string a, string b)
{
	string Tong;
	int l1 = a.size() - 1;
	int l2 = b.size() - 1;
	int Nho = 0;
	for (int i = 0; i <= l1 || i <= l2; i++)
	{
		int x, y, z;
		if (i > l1)
			x = 0;
		else
			x = (a[l1 - i] - 48);
		if (i > l2)
			y = 0;
		else
			y = (b[l2 - i] - 48);

		z = x + y + Nho;

		if (z > 9)
			Nho = 1;
		else
			Nho = 0;
		Tong.push_back(z % 10 + 48);
	}

	if (Nho)
		Tong.push_back('1');
	for (int i = 0; i < Tong.size() / 2; i++)
		swap(Tong[i], Tong[Tong.size() - 1 - i]);
	return Tong;
}

// Hàm chuyển từ nhị phân sang thập phân
string Bin2Dec(string Bin)
{
	string Dec = "0";
	for (int i = Bin.length() - 1; i >= 0; i--)
	{
		if (Bin[i] == '0')
			continue;
		string Temp = "1";
		// Tính giá trị lũy thừa 2 sau đó cộng lại
		for (int j = 0; j < (Bin.length() - 1 - i); j++)
			Temp = StrMult2(Temp);
		Dec = StrAddStr(Dec, Temp);
	}
	return Dec;
}

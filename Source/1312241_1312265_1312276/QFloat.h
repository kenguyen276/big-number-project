﻿#pragma once
#include "Header.h"

class QFloat
{
	int data[4];
public:
	QFloat(void); // Hàm khởi tạo mặc định
	~QFloat(void); // Hàm hủy mặc định

	void Scan(string Str); // Hàm nhập giá trị
	string Print(); // Hàm xuất giá trị

	string DecToBin(); // Hàm chuyển từ thập phân sang nhị phân

	string BinToDec(string Bin); // Hàm chuyển từ nhị phân sang thập phân

	string BinThapPhan(string Str); // Hàm chuyển phần thập phân sang nhị phân
	string BinNguyen(string Str); // Hàm chuyển phần nguyên sang nhị phân

	QFloat operator+(QFloat b);				// Toán tử cộng
	void ShiftSignificantRight(int data);	// Dịch trái phần trị

	QFloat operator-(QFloat b); // Toán tử trừ
	QFloat operator*(QFloat b); // Toán tử nhân
	QFloat operator/(QFloat b); // Toán tử chia

};

